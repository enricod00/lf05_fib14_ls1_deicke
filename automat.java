/* Hallo Herr Zickermann. Bei diesem Code handelt es sich nur um A2.1 bis A2.5. Ich habe den Automaten grundlegend selbst erstellt und dabei einige alternative Methoden benutzt. 
Erst später fiel mir auf, dass wir den wir das Grundgerüst aus Ihrem Repository übernehmen können. Mich würde es freuen, wenn Sie dadurch trotzdem sehen, dass ich mich tiefgehend mit der Aufgabe beschäftigt habe.
Zudem musste ich den Code hier ins Repository direkt einfügen, statt es zu pushen, da ich durch den Verlust meiner Ausbildung meinen neuen Laptop nicht mehr mit Bitbucket verbinden konnte. Die vollständige Abgabe
des Fahrkartenautomaten finden Sie als sepearates Projekt in meinem Repository.
*/

import java.util.Scanner;
public class automat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		float fahrpreis = 0.0f;
		float muenze = 0.0f;
		float minmuenze = 0.5f;
		float maxmuenze = 2.0f;
		float bezahlt = 0.0f;
		String zahlbetrag = "Zu zahlender Betrag (EURO): ";
		String nochzuzahlen = "Noch zu zahlen (EURO): ";
		String eingabe = "Eingabe (mind. 5Ct, höchstens 2 Euro): ";
		String ausgabe = "Fahrschein wird ausgegeben";
		Scanner myScanner = new Scanner(System.in);
		System.out.print(zahlbetrag);
		fahrpreis = myScanner.nextFloat();	
		System.out.println (nochzuzahlen + fahrpreis);
		//Münzen einwerfen
		 do {		
			System.out.print (eingabe);
			muenze = myScanner.nextFloat();
			// Münzeingabe prüfen
			if (muenze >= minmuenze && muenze <= maxmuenze) { 
				fahrpreis -= muenze;	
				fahrpreis = (float) (Math.round(fahrpreis * 100) / 100.0);
			} else {
				System.out.println ("Münze passt nicht!");
			}	
			if (fahrpreis > bezahlt) {
				System.out.println (nochzuzahlen + fahrpreis);	
			} else {
				//System.out.println (fahrpreis);
				System.out.println (ausgabe);
				myScanner.close();
				if (fahrpreis < bezahlt) {
					float restgeld = (fahrpreis + (-fahrpreis * 2));
					System.out.printf ("Der Rückbetrag in Höhe von %.2f EURO wird in folgenden Münzen ausgezahlt:\n" , restgeld);
					while (restgeld > bezahlt)  {
						//System.out.println (fahrpreis);
						if (restgeld >= 1.0f) {
							System.out.println ("1 EURO");
							restgeld -= 1.0f;
							restgeld = (float) (Math.round(restgeld * 100) / 100.0);
						}
						if (restgeld >= 0.5f) {
							System.out.println ("50 Ct");
							restgeld -= 0.5f;	
							restgeld = (float) (Math.round(restgeld * 100) / 100.0);
						}
						if (restgeld >= 0.2f) {
							System.out.println ("20 Ct");
							restgeld -= 0.2f;	
							restgeld = (float) (Math.round(restgeld * 100) / 100.0);
						}
						if (restgeld >= 0.1f) {
							System.out.println ("10 Ct");
							restgeld -= 0.1f;	
							restgeld = (float) (Math.round(restgeld * 100) / 100.0);
						}
						if (restgeld >= 0.05f) {
							System.out.println ("5 Ct");
							restgeld -= 0.05f;	
							restgeld = (float) (Math.round(restgeld * 100) / 100.0);
						}
						if (restgeld == 0) {
							break;
						}
						//System.out.println (restgeld);
					} 
				}
			}
		} while (fahrpreis > bezahlt);
	}
}