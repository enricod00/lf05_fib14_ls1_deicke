
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
System.out.print ("\"Das ist mein Testsatz.\"");
System.out.print (" Hier sehen Sie meinen \nzweiten Testsatz.");
System.out.print ("\n      *\n     ***\n    *****\n   *******\n ***********\n*************\n     ***\n     ***");
// Unterschied zwischen der print()- und der println()-Anweisung: print()- nur der reine Text wird angezeigt, bei println alle Eingaben und Modifikationen innerhalb der Klammer.

 
String s = "22,4234234234";
System.out.printf( "\n%.5s\n", s ); 


System.out.printf( "\n%.2f\n", 111.2222); 

double d = 4.0;
System.out.printf( "\n%.2f\n", d);


System.out.printf ( "\n%.2f\n", 1000000.551);

System.out.printf ( "\n%.2f\n", 97.34);


	}

}
